<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveExtensions[]
ActiveExtensions[]=ezmultiupload
ActiveExtensions[]=eztags
ActiveExtensions[]=ezautosave
ActiveExtensions[]=ezjscore
ActiveExtensions[]=ezstarrating
ActiveExtensions[]=ezgmaplocation
ActiveExtensions[]=ezflow
ActiveExtensions[]=ezwt
ActiveExtensions[]=ezwebin
ActiveExtensions[]=ezie
ActiveExtensions[]=ezoe
ActiveExtensions[]=ezodf
ActiveExtensions[]=ezprestapiprovider

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=eng
SiteList[]
SiteList[]=www
SiteList[]=eng
SiteList[]=administration
SiteList[]=iphone
RootNodeDepth=1

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]
AvailableSiteAccessList[]=www
AvailableSiteAccessList[]=eng
AvailableSiteAccessList[]=administration
AvailableSiteAccessList[]=iphone
MatchOrder=uri
HostMatchMapItems[]

[DesignSettings]
DesignLocationCache=enabled

[RegionalSettings]
TranslationSA[]
TranslationSA[eng]=Eng

[FileSettings]
VarDir=var/ezflow_site

[MailSettings]
Transport=sendmail
AdminEmail=tanta@tantacom.com
EmailSender=

[EmbedViewModeSettings]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]
InlineViewModes[]=embed-inline
*/ ?>