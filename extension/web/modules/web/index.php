<?php

$categories = Category::fetchAll();
foreach( $categories as $category )
{
    $id_cat = $category->attribute( 'id' );

    if ( !CategoryUsers::existsForCurrentUser( $id_cat ) )
    {
        $category_users = CategoryUsers::create( $id_cat );
        $category_users->store();
    }
}


$tpl = eZTemplate::factory();
$Result['content'] = $tpl->fetch( 'design:users/index.tpl' );

?>
