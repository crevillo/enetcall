<?php

if ( !isset( $Params['IdCat'] ) )
    return $Module->handleError( eZError::KERNEL_NOT_FOUND, 'kernel' );

$id_cat = $Params['IdCat'];
$category = Category::fetchById( $id_cat );
if ( !$category )
   return $Module->handleError( eZError::KERNEL_NOT_FOUND, 'kernel' );
 
$sections = Section::fetchByCategory( $id_cat );

foreach( $sections as $section )
{
    $id_secc = $section->attribute( 'id' );

    if ( !SectionUsers::existsForCurrentUser( $id_secc ) )
    {
        $section_users = SectionUsers::create( $id_secc );
        $section_users->store();
    }
}

$tpl = eZTemplate::factory();
$tpl->setVariable( 'category', $category );
$Result['content'] = $tpl->fetch( 'design:users/category.tpl' );

?>
