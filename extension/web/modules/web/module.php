<?php

$Module = array(
    'name' => 'web',
    'variable_params' => true 
);

$ViewList = array();

$ViewList['index'] = array(
    'script' => 'index.php',
    'functions' => 'web'
);

$ViewList['category'] = array(
    'script' => 'category.php',
    'functions' => 'web',
    'params' => array( 'IdCat' )
);

$ViewList['section'] = array(
    'script' => 'section.php',
    'functions' => 'web',
    'params' => array( 'IdSec' )
);

$FunctionList = array();
$FunctionList['web'] = array();

?>
