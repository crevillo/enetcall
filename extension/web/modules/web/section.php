<?php
$tpl = eZTemplate::factory();
if ( !isset( $Params['IdSec'] ) )
    return $Module->handleError( eZError::KERNEL_NOT_FOUND, 'kernel' );

$id_sec = $Params['IdSec'];
$section = Section::fetchById( $id_sec );
if ( !$section )
   return $Module->handleError( eZError::KERNEL_NOT_FOUND, 'kernel' );





$http = eZHTTPTool::instance();
if ( $http->hasPostVariable( 'btnSend' ) )
{
    $validator = new FormValidator();
    $fields = explode( ',', $http->postVariable( 'fields' ) );
    if ( $id_sec == 1 )
    {  
        $customValidator = new validatorSection1();
        $validator->AddCustomValidator( $customValidator );
    }
    else
    {
        foreach( $fields as $field )
        {   
            if ( $http->hasPostVariable( $field ) )
            {    
                if ( substr( $field, 0, 11 ) === 'textarea300' )
                {        
                    $validator->addValidation( $field, "req", 'Input required' );  
                    $validator->addValidation( $field, "maxlen=300", 'Maximum length exceeded for textearea.' );
                }
                if ( substr( $field, 0, 4 ) === 'cost' )
                {            
                    $validator->addValidation( $field, "num", 'Value must be numeric' );
                }
                

                /*if ( substr( $field, 0, 17 ) === 'radioparticipants' )
                {   
                    if( $http->postVariable( $field ) == 1 )
                    {
                        $aux = explode( "_", $field );
                        $textfield = 'participants_' . $aux[1] .  '_' . $aux[2] . '_fromradio' ;
                        if ( $http->hasPostVariable( $textfield ) )
                        {
                            $validator->addValidation( $textfield, "req", 'Input required' );
                        }
                    }
                }*/
            }
        }       

    }

    $validator->ValidateForm();
    $error_hash = $validator->GetErrors();    
   
    $tpl->setVariable( 'errors', $error_hash );
    // si no hay errores
    if ( count( $error_hash ) === 0 )
    {
        // recorremos los fields de nuevo y guardamos los que no hayan tenido error
        $posted_data = array();
        $cost = 0;
        foreach( $fields as $field )
        {
            if ( !isset( $error_hash[$field] ) )
            {
                if ( substr( $field, 0, 4 ) == 'cost' )
                    $cost += $http->postVariable( $field );
                $posted_data[$field] = $http->postVariable( $field );
            }
        } 

        if ( count( $posted_data ) )
        {
            $answer = serialize( $posted_data );
            $answeruser = AnswersUsers::create( $id_sec, $answer );
            $answeruser->store();

            // actualizar el coste de la seccion 
            $sectionUser = SectionUsers::sectionByIdAndUser( $id_sec, eZUser::currentUser()->id() );
            $sectionUser->updateCost( $cost );
            $sectionUser->updateStatus();
            $sectionUser->store();

            // actualizar el coste de la categoría 
            $category = CategoryUsers::fetchByUserAndId( eZUser::currentUser()->id(), $section->attribute( 'id_cat' ) );
            $category->updateCost();
            $category->updateStatus();
            $category->store();
        }
    }
}

$sectionuser = SectionUsers::sectionByIdAndUser( $id_sec, eZUser::currentUser()->id() );
$status = $sectionuser->attribute( 'status' );
$tpl->setVariable( 'status', $status );
if ( $status == 1 )
{
   
    $answerUser = AnswersUsers::fetchForUserAndId( $id_sec );

    $tpl->setVariable( 'answer', unserialize( $answerUser->attribute( 'answer' ) ) );
}


$category = Category::fetchById( $section->attribute( 'id_cat' ) );

$tpl->setVariable( 'category', $category );
$tpl->setVariable( 'section', $section );
$Result['content'] = $tpl->fetch( 'design:users/sections/' . $id_sec . '.tpl' );

?>
