<?php

$FunctionList = array();

$FunctionList['get_categories'] = array(
    'name' => 'get_categories',
    'operation_types' => array( 'read' ),
    'call_method' => array(
        'class'  => 'webFunctionCollection',
        'method' => 'getCategories'
    ),
    'parameter_type'  => 'standard',
    'parameters' => array() 
);

$FunctionList['get_categories_for_user'] = array(
    'name' => 'get_categories_for_user',
    'operation_types' => array( 'read' ),
    'call_method' => array(
        'class'  => 'webFunctionCollection',
        'method' => 'getCategoriesForUser'
    ),
    'parameter_type'  => 'standard',
    'parameters' => array() 
);

$FunctionList['get_sections_for_user_and_cat'] = array(
    'name' => 'get_sections_for_user_and_cat',
    'operation_types' => array( 'read' ),
    'call_method' => array(
        'class'  => 'webFunctionCollection',
        'method' => 'getSectionsForUserAndCat'
    ),
    'parameter_type'  => 'standard',
    'parameters' => array(
        array( 
            'name' => 'id_cat',
            'type' => 'integer',
            'required' => true 
        ),
    ) 
);

$FunctionList['country_list'] = array(
    'name' => 'country',
    'operation_types' => array( 'read' ),
    'call_method' => array( 
        'class' => 'webFunctionCollection',
        'method' => 'fetchCountryList' 
    ),
    'parameter_type' => 'standard',
    'parameters' => array(
         array( 
             'name' => 'filter',
             'type' => 'string',
             'required' => false 
         ),
         array(
            'name' => 'value',
            'type' => 'string',
            'required' => false     
        ) 
    )
);

?>
