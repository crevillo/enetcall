<?php
class AnswersUsers extends eZPersistentObject
{
    function AnswersUsers( $row )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        static $definition = array(
            'fields' => array(
                'id_sec' => array( 
                    'name' => 'id_sec',
                    'datatype' => 'integer',
                    'default' => 0,
                    'required' => true 
                ),                
                'user_id' => array(
                    'name' => 'user_id',
                    'datatype' => 'integer',
                    'default' => 0,
                    'required' => true
                ),
                'answer' => array(
                    'name' => 'answer',
                    'datatype' => 'string',
                    'default' => '',
                    'required' => false,
                )               
            ),            
            'keys' => array( 'id_sec', 'user_id' ),            
            'class_name' => 'AnswersUsers',
            'name' => 'users_answers' 
        );

        return $definition;
    }

    /**
     * Adds the entry for the sec and the user
     * It creates but it doesn't store (!) 
     *
     * @param int $id_sec
     * @param string $anwser
     * @return AnswersUsers
     */
    static function create( $id_sec, $answer )
    {
        return new AnswersUsers( array(
            'id_sec' => $id_sec,
            'user_id' => eZUser::currentUser()->id(),
            'answer' => $answer,
        ) );                                    
    }

    static function fetchForUserAndId( $id_sec )
    {
        return eZPersistentObject::fetchObject( 
            self::definition(),
            null,
            array( 'id_sec' => $id_sec, 'user_id' => eZUser::currentUserId() )
        );
    }
   
 
}
?>
