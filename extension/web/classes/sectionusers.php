<?php
class SectionUsers extends eZPersistentObject
{
    const STATUS_EMPTY = 0;
    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;
    const STATUS_DENIED = 3;
    
    function SectionUsers( $row )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        static $definition = array(
            'fields' => array(
                'id_sec' => array( 
                    'name' => 'id_sec',
                    'datatype' => 'integer',
                    'default' => 0,
                    'required' => true 
                ),
                'user_id' => array(
                    'name' => 'user_id',
                    'datatype' => 'string',
                    'default' => 0,
                    'required' => true
                ),
                'cost' => array(
                    'name' => 'cost',
                    'datatype' => 'float',
                    'default' => 0,
                    'required' => false,
                ),
                'status' => array(
                    'name' => 'status',
                    'datatype' => 'integer',
                    'default' => 0,
                    'required' => true
                )
            ),
            'function_attributes' => array(
                'section_name' => 'sectionName',
            ),
            'keys' => array( 'id_sec', 'user_id' ),            
            'class_name' => 'SectionUsers',
            'name' => 'sections_users' 
        );

        return $definition;
    }

    /**
     * Check if the section is created for the current user
     *
     * @param int $id_sec
     * @return bool
     */
    static function existsForCurrentUser( $id_sec )
    {
        $existence = eZPersistentObject::fetchObject( 
            self::definition(),
            null,
            array( 
                'id_sec' => $id_sec,
                'user_id' => eZUser::currentUser()->id()
            )
        );

        return ( count( $existence ) > 0 );
    }
    
    /**
     * Adds the entry for the sec and the user. Cost and Status will be 0
     * It creates but it doesn't store (!) 
     *
     * @param int $id_cat
     * @return CategoryUsers
     */
    static function create( $id_sec )
    {
        return new SectionUsers( array(
            'id_sec' => $id_sec,
            'user_id' => eZUser::currentUser()->id(),
            'cost' => 0,
            'status' => CategoryUsers::STATUS_EMPTY
        ) );                                    
    }

    function sectionName()
    {
        $sec = Section::fetchById( $this->id_sec );
        return $sec->attribute( 'name' );       
    }

    static function sectionByIdAndUser( $id_sec, $user_id )
    {
        return eZPersistentObject::fetchObject(
            self::definition(),
            null,
            array( 'id_sec' => $id_sec, 'user_id' => $user_id )
        );   
    }

    static function fetchByUser( $user_id )
    {  
        return eZPersistentObject::fetchObjectList(
            self::definition(),
            null,
            array( 'user_id' => $user_id )
        );
    }

    static function fetchByUserAndCat( $user_id, $id_cat )
    {  
        return eZPersistentObject::fetchObjectList(
            self::definition(),
            null,
            array( 'user_id' => $user_id ),
            null,
            null,
            true,
            null,
            null,
            array( 'sections' ),
            ' AND sections.id = sections_users.id_sec AND sections.id_cat = ' . $id_cat
        );
    }

    function updateCost( $cost )
    {
        $this->cost = $cost;
    }

    function updateStatus()
    {
        $this->status = 1;
    }
 
}
?>
