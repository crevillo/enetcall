<?php
class Category extends eZPersistentObject
{
    function Category( $row )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        static $definition = array(
            'fields' => array(
                'id' => array( 
                    'name' => 'id',
                    'datatype' => 'integer',
                    'default' => 0,
                    'required' => true 
                ),
                'name' => array(
                    'name' => 'name',
                    'datatype' => 'string',
                    'default' => '',
                    'required' => true
                )
            ),
            'keys' => 'id',           
            
            'increment_key' => 'id',
            'class_name' => 'Category',
            'sort' => array( 'id' => 'asc' ),
            'name' => 'categories' 
        );

        return $definition;
    }

    static function fetchAll()
    {
        return eZPersistentObject::fetchObjectList( self::definition() );
    }   

    static function fetchById( $id )
    {
        return eZPersistentObject::fetchObject(
            self::definition(),
            null,
            array( 'id' => $id )
        );
    }

    
}
?>
