<?php
class validatorSection1 extends CustomValidator
{
    function DoValidate(&$formars,&$error_hash)
    {
        $c1 = 0;
        if ( isset( $formars['selmax5_1_1'] ) )
            $c1 += count( $formars['selmax5_1_1'] );
        if ( $formars['textarea_1_1_8'] != '' )
            $c1 ++;
        if ( $c1 > 5 )
             $error_hash['first_group'] = "Please select a maximum of 5 options";
        
        if ( ( $c1 == 0 ) && !isset( $formars['radioparticipants_1_2'] ) && !isset( $formars['radioparticipants_1_4'] ) )
        {
            $error_hash['general'] = "Please fill at least one question";
           // return false;
        }

        if ( ( ( $formars['cost_1_travel'] == '' ) || !is_numeric( $formars['cost_1_travel'] ) ) &&
            ( ( $formars['cost_1_personal'] == '' ) || !is_numeric( $formars['cost_1_personal'] ) ) &&
            ( ( $formars['cost_1_other'] == '' ) || !is_numeric( $formars['cost_1_other'] ) ) &&
            ( ( $formars['cost_1_subcontracting'] == '' ) || !is_numeric( $formars['cost_1_subcontracting'] ) ) &&
            ( ( $formars['cost_1_indirect'] == '' ) || !is_numeric( $formars['cost_1_indirect'] ) )
        )
            $error_hash['cost'] = "Please add cost to your entry";
        
        if ( isset( $formars['radioparticipants_1_2'] ) && 
             ( $formars['radioparticipants_1_2'] == 1 ) &&
             ( $formars['participants_1_2_fromradio']  == '' ) )
            $error_hash['second_group'] = 'Input required';

         if ( isset( $formars['radioparticipants_1_4'] ) && 
             ( $formars['radioparticipants_1_4'] == 1 ) &&
             ( $formars['participants_1_4_fromradio']  == '' ) )
            $error_hash['third_group'] = 'Input required';

        return count( $error_hash ) > 0 ;
    }
}
?>
