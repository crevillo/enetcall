<?php
class Section extends eZPersistentObject
{
    function Section( $row )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        static $definition = array(
            'fields' => array(
                'id' => array( 
                    'name' => 'id',
                    'datatype' => 'integer',
                    'default' => 0,
                    'required' => true 
                ),
                'name' => array(
                    'name' => 'name',
                    'datatype' => 'string',
                    'default' => '',
                    'required' => true
                ),
                'id_cat' => array(
                    'name' => 'id_cat',
                    'datatype' => 'integer',
                    'default' => 0,
                    'required' => true,
                    'foreign_class' => 'Category',
                    'foreign_attribute' => 'id',
                    'multiplicity' => '1..*'
                )
            ),
            
            'increment_key' => 'id',
            'class_name' => 'Section',
            'sort' => array( 'id' => 'asc' ),
            'name' => 'sections' 
        );

        return $definition;
    }

    static function fetchByCategory( $id_cat )
    {
        return eZPersistentObject::fetchObjectList(
            self::definition(),
            null,
            array( 'id_cat' => $id_cat )
        );
    }

    static function fetchById( $id )
    {
        return eZPersistentObject::fetchObject(
            self::definition(),
            null,
            array( 'id' => $id )
        );
    }
}
?>
