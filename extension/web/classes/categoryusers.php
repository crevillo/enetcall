<?php
class CategoryUsers extends eZPersistentObject
{
    const STATUS_EMPTY = 0;
    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;
    const STATUS_DENIED = 3;
    
    function CategoryUsers( $row )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        static $definition = array(
            'fields' => array(
                'id_cat' => array( 
                    'name' => 'id_cat',
                    'datatype' => 'integer',
                    'default' => 0,
                    'required' => true 
                ),
                'user_id' => array(
                    'name' => 'user_id',
                    'datatype' => 'string',
                    'default' => 0,
                    'required' => true
                ),
                'cost' => array(
                    'name' => 'cost',
                    'datatype' => 'float',
                    'default' => 0,
                    'required' => false,
                ),
                'status' => array(
                    'name' => 'status',
                    'datatype' => 'integer',
                    'default' => 0,
                    'required' => true
                )
            ),
            'function_attributes' => array(
                'category_name' => 'categoryName',
            ),
            'keys' => array( 'id_cat', 'user_id' ),            
            'class_name' => 'CategoryUsers',
            'name' => 'categories_users' 
        );

        return $definition;
    }

    /**
     * Check if the category is created for the current user
     *
     * @param int $id_cat
     * @return bool
     */
    static function existsForCurrentUser( $id_cat )
    {
        $existence = eZPersistentObject::fetchObject( 
            self::definition(),
            null,
            array( 
                'id_cat' => $id_cat,
                'user_id' => eZUser::currentUser()->id()
            )
        );

        return ( count( $existence ) > 0 );
    }
    
    /**
     * Adds the entry for the cat and the user. Cost and Status will be 0
     * It creates but it doesn't store (!) 
     *
     * @param int $id_cat
     * @return CategoryUsers
     */
    static function create( $id_cat )
    {
        return new CategoryUsers( array(
            'id_cat' => $id_cat,
            'user_id' => eZUser::currentUser()->id(),
            'cost' => 0,
            'status' => CategoryUsers::STATUS_EMPTY
        ) );                                    
    }

    function categoryName()
    {
        $cat = Category::fetchById( $this->id_cat );
        return $cat->attribute( 'name' );       
    }

    static function fetchByUser( $user_id )
    {         
        return eZPersistentObject::fetchObjectList(
            self::definition(),
            null,
            array( 'user_id' => $user_id )
        );
    }

    static function fetchByUserAndId( $user_id, $id_cat )
    {         
        return eZPersistentObject::fetchObject(
            self::definition(),
            null,
            array( 'user_id' => $user_id, 'id_cat' => $id_cat )
        );
    }

    function sectionsByUser()
    {
        $sections = SectionUsers::fetchByUserAndCat( $this->user_id, $this->id_cat ); 
        return $sections;
    }

    function updateCost()
    {
        $sections = $this->sectionsByUser();
        $cost = 0;
        foreach( $sections as $section )
        {
            $cost += $section->attribute( 'cost' );
        }
        $this->cost = $cost;
    }

    function updateStatus()
    {
        $this->status = 1;
    }
 
}
?>
