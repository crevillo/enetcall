<?php 
class webFunctionCollection{
    
    static function getCategories()
    {
        return array( 'result' => Category::fetchAll() );
    }

    static function getCategoriesForUser()
    {
        return array( 'result' => CategoryUsers::fetchByUser( eZUser::currentUser()->id() ) );
    }

    static function getSectionsForUser()
    {
        return array( 'result' => SectionUsers::fetchByUser( eZUser::currentUser()->id() ) );
    }

    static function getSectionsForUserAndCat( $id_cat )
    {
        return array( 'result' => SectionUsers::fetchByUserAndCat( eZUser::currentUser()->id(), $id_cat ) );
    }

    static public function fetchCountryList( $filter, $value )
    {
        // Fetch country list
        if ( !$filter and !$value )
        {
            $country = eZEnetCountryType::fetchCountryList();
        }
        else
        {
            $country = eZEnetCountryType::fetchCountry( $value, $filter );
        }

        return array( 'result' => $country );
    }

}
?>
