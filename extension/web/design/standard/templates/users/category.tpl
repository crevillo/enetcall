<h1>{$category.name}</h1>

<table>
    <thead>
        <th>Category</th>
        <th>Cost</th>
        <th>Status</th>
    </thead>
    {def $data = fetch( 'web', 'get_sections_for_user_and_cat', hash( 'id_cat', $category.id ))}
    <tbody>
    {foreach $data as $secc}
        <tr>
            <td><a href={concat( "web/section/", $secc.id_sec)|ezurl}>{$secc.section_name|wash}</a><td>
            <td>{$secc.cost}<td>
            <td>{$secc.status}<td>
        <tr>
    {/foreach}
    </tbody>
</table>
