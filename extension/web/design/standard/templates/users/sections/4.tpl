{def $fields = array( concat('textarea300_', $section.id, '_1' ),
                      concat( 'cost_', $section.id, '_travel' ),
                      concat( 'cost_', $section.id, '_personal' ),
                      concat( 'cost_', $section.id, '_other' ),
                      concat( 'cost_', $section.id, '_subcontracting' ),
                      concat( 'cost_', $section.id, '_indirect' )                    
)}
<div class="border-box">
<div class="border-tl"><div class="border-tr"><div class="border-tc"></div></div></div>
<div class="border-ml"><div class="border-mr"><div class="border-mc float-break">
<h1>{$category.name}</h1>
<h2>{$section.name}</h2>

<form action={concat( "web/section/", $section.id )|ezurl} method="post">
<div class="block">
<fieldset>
<legend>
   Briefly explain activity/ies</legend>


   <input type="hidden" name="fields" value="{$fields|implode(',')}" /> 
   {if and( is_set( $errors ), is_set( $errors[concat( 'textarea300_', $section.id, '_1' )]) )}
   <p class="msgerror">{$errors[concat( 'textarea300_', $section.id, '_1' )]}</p>
{/if}
   <textarea name="textarea300_{$section.id}_1" rows="7" cols="50" {if and( is_set( $errors ), is_set( $errors[concat( 'textarea300_', $section.id, '_1' )]) )}class="error"{/if} {if $status|eq(1)}readonly="yes"{/if}>{if is_set( $answer[concat( 'textarea300_', $section.id, '_1' )] )}{$answer[concat( 'textarea300_', $section.id, '_1' )]}{/if}</textarea>
</fieldset>

<fieldset>
    <legend>
        Costes desglosados en
    </legend>
    <label for="cost_{$section.id}_travel">
       Travel <input type="text" name="cost_{$section.id}_travel" id="cost_{$section.id}_travel" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_travel' )] )}value="{$answer[concat( 'cost_', $section.id, '_travel' )]}"{/if} /> </label>
<label for="cost_{$section.id}_personal">
Personal <input type="text" name="cost_{$section.id}_personal" id="cost_{$section.id}_personal" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_personal' )] )}value="{$answer[concat( 'cost_', $section.id, '_personal' )]}"{/if}/> </label>
<label for="cost_{$section.id}_other">
Other<input type="text" name="cost_{$section.id}_other" id="cost_{$section.id}_other" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_other' )] )}value="{$answer[concat( 'cost_', $section.id, '_other' )]}"{/if}/> </label>
<label for="cost_{$section.id}_subcontracting">
Subcontracting <input type="text" name="cost_{$section.id}_subcontracting" id="cost_{$section.id}_subcontracting" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_subcontracting' )] )}value="{$answer[concat( 'cost_', $section.id, '_subcontracting' )]}"{/if}/> </label>
<label for="cost_{$section.id}_indirect">
Indirect Cost <input type="text" name="cost_{$section.id}_indirect" id="cost_$section.id}_indirect" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_indirect' )] )}value="{$answer[concat( 'cost_', $section.id, '_indirect' )]}"{/if} /> </label>

<label for="cost_{$section.id}_total">
<strong>Total</strong> <input type="text" name="cost_{$section.id}_total" id="cost_$section.id}_total" readonly="yes"/></label>


</fieldset>
</div>
<div class="block">
     <input type="submit" value="Send" name="btnSend" {if $status|eq(1)}disabled="disabled"{/if}/>
    <input type="submit" value="Cancel" name="btnReset" />
</div>
</form>
</div></div></div>
<div class="border-bl"><div class="border-br"><div class="border-bc"></div></div></div>
</div>
