
{ezscript_require( array( 'ezjsc::jquery', 'users/sections/1.js' ) )}
{def $fields = array( concat('selmax5_', $section.id, '_1' ),
                      concat('textarea_', $section.id, '_1_8' ),
                      concat('radioparticipants_', $section.id, '_2' ),
                      concat('radioparticipants_', $section.id, '_4' ),
                      concat('participants_', $section.id, '_2_fromradio' ),
                      concat('participants_', $section.id, '_4_fromradio' ),
                      concat( 'cost_', $section.id, '_travel' ),
                      concat( 'cost_', $section.id, '_personal' ),
                      concat( 'cost_', $section.id, '_other' ),
                      concat( 'cost_', $section.id, '_subcontracting' ),
                      concat( 'cost_', $section.id, '_indirect' )                    
)}

<div class="border-box">
<div class="border-tl"><div class="border-tr"><div class="border-tc"></div></div></div>
<div class="border-ml"><div class="border-mr"><div class="border-mc float-break">
<h1>{$category.name}</h1>
<h2>{$section.name}</h2>

{if and( is_set( $errors ), is_set( $errors['general'] ) )}
    <p class="msgerror">
        {$errors.general}
    </p>
{/if}

<form action={concat( "web/section/", $section.id )|ezurl} method="post">
<input type="hidden" name="fields" value="{$fields|implode(',')}" /> 
<div class="block">
<fieldset>
<legend>
    Please choose among the following topics the ones you would like to be addressed during  the training sessions (Maximum 5):</legend>

    {if and( is_set( $errors ), is_set( $errors['first_group']) )}
   <p class="msgerror" id="field_1_error">{$errors['first_group']}</p>
{/if}

    <label for="field_{$section.id}_1_0">
        <input type="checkbox" name="selmax5_{$section.id}_1[]" id="field_{$section.id}_1_0" value="0" {if $status|eq(1)}readonly="yes"{/if} {if or( $answer[concat( 'selmax5_', $section.id, '_1' )]|contains(0), 
    and( ezhttp_hasvariable( concat( 'selmax5_', $section.id, '_1' ), 'post' ), ezhttp( concat( 'selmax5_', $section.id, '_1' ), 'post' )|contains( 0) ) )
}checked="checked"{/if} /> Promotion
    </label>
     <label for="field_{$section.id}_1_1">
<input type="checkbox" name="selmax5_{$section.id}_1[]" id="field_{$section.id}_1_1" value="1" {if $status|eq(1)}readonly="yes"{/if} {if or( $answer[concat( 'selmax5_', $section.id, '_1' )]|contains(1), 
    and( ezhttp_hasvariable( concat( 'selmax5_', $section.id, '_1' ), 'post' ), ezhttp( concat( 'selmax5_', $section.id, '_1' ), 'post' )|contains( 1 ) ) )
}checked="checked"{/if}/> Collaboration with industry
     </label>
 <label for="field_{$section.id}_1_2">
    <input type="checkbox" name="selmax5_{$section.id}_1[]" id="field_{$section.id}_1_2" value="2"{if $status|eq(1)}readonly="yes"{/if} {if or( $answer[concat( 'selmax5_', $section.id, '_1' )]|contains(2), 
    and( ezhttp_hasvariable( concat( 'selmax5_', $section.id, '_1' ), 'post' ), ezhttp( concat( 'selmax5_', $section.id, '_1' ), 'post' )|contains( 2 ) ) )
}checked="checked"{/if} / >Euraxess Handbook
</label>

<label for="field_{$section.id}_1_3">
    <input type="checkbox" name="selmax5_{$section.id}_1[]" id="field_{$section.id}_1_3" value="3" {if $status|eq(1)}readonly="yes"{/if} {if or( $answer[concat( 'selmax5_', $section.id, '_1' )]|contains(3), 
    and( ezhttp_hasvariable( concat( 'selmax5_', $section.id, '_1' ), 'post' ), ezhttp( concat( 'selmax5_', $section.id, '_1' ), 'post' )|contains( 3 ) ) )
}checked="checked"{/if} /> Intercultural communication
</label>

 <label for="field_{$section.id}_1_4">
<input type="checkbox" name="selmax5_{$section.id}_1[]" id="field_{$section.id}_1_4" value="4" {if $status|eq(1)}readonly="yes"{/if} {if or( $answer[concat( 'selmax5_', $section.id, '_1' )]|contains(4), 
    and( ezhttp_hasvariable( concat( 'selmax5_', $section.id, '_1' ), 'post' ), ezhttp( concat( 'selmax5_', $section.id, '_1' ), 'post' )|contains( 4 ) ) )
}checked="checked"{/if}} /> Horizon 2020
</label>
 <label for="field_{$section.id}_1_5">
<input type="checkbox" name="selmax5_{$section.id}_1[]" id="field_{$section.id}_1_5" value="5" {if $status|eq(1)}readonly="yes"{/if} {if or( $answer[concat( 'selmax5_', $section.id, '_1' )]|contains(5), 
    and( ezhttp_hasvariable( concat( 'selmax5_', $section.id, '_1' ), 'post' ), ezhttp( concat( 'selmax5_', $section.id, '_1' ), 'post' )|contains( 5 ) ) )
}checked="checked"{/if}  /> Social security / Visa package
</label>
 <label for="field_{$section.id}_1_6">
<input type="checkbox" name="selmax5_{$section.id}_1[]" id="field_{$section.id}_1_6" value="6" {if $status|eq(1)}readonly="yes"{/if} {if or( $answer[concat( 'selmax5_', $section.id, '_1' )]|contains(6), 
    and( ezhttp_hasvariable( concat( 'selmax5_', $section.id, '_1' ), 'post' ), ezhttp( concat( 'selmax5_', $section.id, '_1' ), 'post' )|contains( 6 ) ) )
}checked="checked"{/if}/> Open recruitment and portability of grants
</label>
 <label for="field_{$section.id}_1_7">
<input type="checkbox" name="selmax5_{$section.id}_1[]" id="field_{$section.id}_1_7" value="7" {if $status|eq(1)}readonly="yes"{/if} {if or( $answer[concat( 'selmax5_', $section.id, '_1' )]|contains(7), 
    and( ezhttp_hasvariable( concat( 'selmax5_', $section.id, '_1' ), 'post' ), ezhttp( concat( 'selmax5_', $section.id, '_1' ), 'post' )|contains( 7 ) ) )
}checked="checked"{/if} /> Use of the European portal / Euraxess extranet
</label>

<label for="field_{$section.id}_1_8">
Suggestion</label><br />
{if and( is_set( $errors ), is_set( $errors[concat( 'textarea_', $section.id, '_1_8' )]) )}
   <p class="msgerror" id="error_textarea_1">{$errors[concat( 'textarea_', $section.id, '_1_8' )]}</p>
{/if}
 <textarea id="textarea_1" name="textarea_{$section.id}_1_8" rows="7" cols="50" {if and( is_set( $errors ), is_set( $errors[concat( 'textarea_', $section.id, '_1_8' )]) )}class="error"{/if} {if $status|eq(1)}readonly="yes"{/if}>{if is_set( $answer[concat( 'textarea_', $section.id, '_1_8' )] )}{$answer[concat( 'textarea_', $section.id, '_1_8' )]}{else if 
    ezhttp_hasvariable( concat( 'textarea_', $section.id, '_1_8' ), 'post' )}{ezhttp( concat( 'textarea_', $section.id, '_1_8' ), 'post' )}{/if}</textarea>


</fieldset>
<fieldset>
    <legend>
        2-day training session for EURAXESS Services members in Budapest (2013)
    </legend>

    <label for="field_{$section.id}_2_1">
    <input type="radio" name="radioparticipants_{$section.id}_2" id="field_{$section.id}_2_1" value="1" class="radio-yes"  {if or( and(is_set( $answer[concat('radioparticipants_', $section.id, '_2')]), $answer[concat('radioparticipants_', $section.id, '_2')]|eq(1)),and( ezhttp_hasvariable( concat( 'radioparticipants_', $section.id, '_2'), 'post' ), ezhttp( concat( 'radioparticipants_', $section.id, '_2'), 'post' )|eq(1)))}checked="checked"{/if} {if $status|eq(1)}disabled="disabled"{/if}/> Yes
    </label>
    <label for="field_{$section.id}_2_2">
    <input type="radio" name="radioparticipants_{$section.id}_2" id="field_{$section.id}_2_2" value="0" class="radio-no" {if or( and(is_set( $answer[concat('radioparticipants_', $section.id, '_2')]), $answer[concat('radioparticipants_', $section.id, '_2')]|eq(0)),and( ezhttp_hasvariable( concat( 'radioparticipants_', $section.id, '_2'), 'post' ), ezhttp( concat( 'radioparticipants_', $section.id, '_2'), 'post' )|eq(0)))}checked="checked"{/if} {if $status|eq(1)}disabled="disabled"{/if}/> No
    </label>
{if and( is_set( $errors ), is_set( $errors['second_group']) )}
   <p class="msgerror">{$errors['second_group']}</p>
{/if}
    <label for="field_{$section.id}_3">
    <input type="text" name="participants_{$section.id}_2_fromradio" id="field_{$section.id}_3" {if or( and(is_set( $answer[concat('radioparticipants_', $section.id, '_2')]), $answer[concat('radioparticipants_', $section.id, '_2')]|eq(1)),and( ezhttp_hasvariable( concat( 'radioparticipants_', $section.id, '_2'), 'post' ), ezhttp( concat( 'radioparticipants_', $section.id, '_2'), 'post' )|eq(1)))|not}disabled="disabled"{/if} {if $status|eq(1)}readonly="yes"{/if} class="participants" value="{if  ezhttp_hasvariable( 'participants_1_2_fromradio', 'post' ) }{ezhttp( 'participants_1_2_fromradio', 'post' ) }{else if is_set( $answer['participants_1_2_fromradio'])}{$answer['participants_1_2_fromradio']}{/if}" /> Number of participants
    </label>
</fieldset>

<fieldset>
    <legend>
        2-day training session for EURAXESS Services members in Barcelona (2014)
    </legend>
    <label for="field_{$section.id}_4_1">
    <input type="radio" name="radioparticipants_{$section.id}_4" id="field_{$section.id}_4_1" value="1" class="radio-yes"  {if or( and(is_set( $answer[concat('radioparticipants_', $section.id, '_4')]), $answer[concat('radioparticipants_', $section.id, '_4')]|eq(1)),and( ezhttp_hasvariable( concat( 'radioparticipants_', $section.id, '_4'), 'post' ), ezhttp( concat( 'radioparticipants_', $section.id, '_4'), 'post' )|eq(1)))}checked="checked"{/if} {if $status|eq(1)}disabled="disabled"{/if}/> Yes
    </label>
    <label for="field_{$section.id}_4_2">
    <input type="radio" name="radioparticipants_{$section.id}_4" id="field_{$section.id}_4_2" value="0" class="radio-no" {if or( and(is_set( $answer[concat('radioparticipants_', $section.id, '_4')]), $answer[concat('radioparticipants_', $section.id, '_4')]|eq(0)),and( ezhttp_hasvariable( concat( 'radioparticipants_', $section.id, '_4'), 'post' ), ezhttp( concat( 'radioparticipants_', $section.id, '_4'), 'post' )|eq(0)))}checked="checked"{/if} {if $status|eq(1)}disabled="disabled"{/if}/> No
    </label>
{if and( is_set( $errors ), is_set( $errors['third_group']) )}
   <p class="msgerror">{$errors['third_group']}</p>
{/if}
    <label for="field_{$section.id}_5">
    <input type="text" name="participants_{$section.id}_4_fromradio" id="field_{$section.id}_5" {if or( and(is_set( $answer[concat('radioparticipants_', $section.id, '_4')]), $answer[concat('radioparticipants_', $section.id, '_4')]|eq(1)),and( ezhttp_hasvariable( concat( 'radioparticipants_', $section.id, '_4'), 'post' ), ezhttp( concat( 'radioparticipants_', $section.id, '_4'), 'post' )|eq(1)))|not}disabled="disabled"{/if} {if $status|eq(1)}readonly="yes"{/if} class="participants" value="{if  ezhttp_hasvariable( 'participants_1_4_fromradio', 'post' ) }{ezhttp( 'participants_1_4_fromradio', 'post' ) }{else if is_set( $answer['participants_1_4_fromradio'])}{$answer['participants_1_4_fromradio']}{/if}" /> Number of participants
    </label>
</fieldset>

<fieldset>
    <legend>
        Costes desglosados en
    </legend>

    {if and( is_set( $errors ), is_set( $errors['cost'] ) )}
    <p class="msgerror">
        {$errors.cost}
    </p>
    {/if}
     
    <label for="cost_{$section.id}_travel">
       Travel <input type="text" name="cost_{$section.id}_travel" id="cost_{$section.id}_travel" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_travel' )] )}value="{$answer[concat( 'cost_', $section.id, '_travel' )]}"{/if} /> </label>
<label for="cost_{$section.id}_personal">
Personal <input type="text" name="cost_{$section.id}_personal" id="cost_{$section.id}_personal" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_personal' )] )}value="{$answer[concat( 'cost_', $section.id, '_personal' )]}"{/if}/> </label>
<label for="cost_{$section.id}_other">
Other<input type="text" name="cost_{$section.id}_other" id="cost_{$section.id}_other" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_other' )] )}value="{$answer[concat( 'cost_', $section.id, '_other' )]}"{/if}/> </label>
<label for="cost_{$section.id}_subcontracting">
Subcontracting <input type="text" name="cost_{$section.id}_subcontracting" id="cost_{$section.id}_subcontracting" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_subcontracting' )] )}value="{$answer[concat( 'cost_', $section.id, '_subcontracting' )]}"{/if}/> </label>
<label for="cost_{$section.id}_indirect">
Indirect Cost <input type="text" name="cost_{$section.id}_indirect" id="cost_$section.id}_indirect" {if $status|eq(1)}readonly="yes"{/if} {if is_set( $answer[concat( 'cost_', $section.id, '_indirect' )] )}value="{$answer[concat( 'cost_', $section.id, '_indirect' )]}"{/if} /> </label>

<label for="cost_{$section.id}_total">
<strong>Total</strong> <input type="text" name="cost_{$section.id}_total" id="cost_$section.id}_total" readonly="yes"/></label>


</fieldset>
</div>
<div class="block">
    <input type="submit" value="Send" name="btnSend" {if $status|eq(1)}disabled="disabled"{/if} />
    <input type="submit" value="Cancel" name="btnReset" />
</div>
</form>
</div></div></div>
<div class="border-bl"><div class="border-br"><div class="border-bc"></div></div></div>
</div>
