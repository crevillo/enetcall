<div class="border-box">
<div class="border-tl"><div class="border-tr"><div class="border-tc"></div></div></div>
<div class="border-ml"><div class="border-mr"><div class="border-mc float-break">
<h1>{$category.name}</h1>
<h2>{$section.name}</h2>
<p>
Indicate whether funding requested within this part of the call will be used to cover the travel costs for participation in the network-wide Portal administrators trainings organised within the T.O.P. II project.</p>
<form action={concat( "web/section/", $section.id )|ezurl} method="post">
<div class="block">
<fieldset>
<legend>
   1-day content oriented training session in XXX (October 2012)</legend>

    
    </legend>
    <label for="field_{$section.id}_1_1">
    <input type="radio" name="field_{$section.id}_1" id="field_{$section.id}_1_1" value="1" /> Yes
    </label>
    <label for="field_{$section.id}_1_2">
    <input type="radio" name="field_{$section.id}_1" id="field_{$section.id}_1_2" value="0" /> No
    </label>   
</fieldset>



<fieldset>
<legend>
   1-day technicaly oriented training session in XXX (2013)</legend>

    
    </legend>
    <label for="field_{$section.id}_2_1">
    <input type="radio" name="field_{$section.id}_2" id="field_{$section.id}_2_1" value="1" /> Yes
    </label>
    <label for="field_{$section.id}_2_2">
    <input type="radio" name="field_{$section.id}_2" id="field_{$section.id}_2_2" value="0" /> No
    </label>   
</fieldset>

<fieldset>
    <legend>
        1-day training session in XXX (2014)
    </legend>
    <label for="field_{$section.id}_3_1">
    <input type="radio" name="field_{$section.id}_3" id="field_{$section.id}_3_1" value="1" /> Yes
    </label>
    <label for="field_{$section.id}_3_2">
    <input type="radio" name="field_{$section.id}_3" id="field_{$section.id}_3_2" value="0" /> No
    </label>
</fieldset>

<fieldset>
    In case that you do not intend using Network Call to cover the travel costs for participation in any of the capacity building trainings,  please specify the assumed funding sources that shall be used to cover these costs  

    <label><input type="text" name="field_{$section.id}_4" /></label>
</fieldset>

<fieldset>
    <legend>
        Costes desglosados en
    </legend>
    <label for="cost_{$section.id}_travel">
       Travel <input type="text" name="cost_{$section.id}_travel" id="cost_{$section.id}_travel" /> </label>
<label for="cost_{$section.id}_personal">
Personal <input type="text" name="cost_{$section.id}_personal" id="cost_{$section.id}_personal" /> </label>
<label for="cost_{$section.id}_other">
Other<input type="text" name="cost_{$section.id}_other" id="cost_{$section.id}_other" /> </label>
<label for="cost_{$section.id}_subcontracting">
Subcontracting <input type="text" name="cost_{$section.id}_subcontracting" id="cost_{$section.id}_subcontracting" /> </label>
<label for="cost_{$section.id}_indirect">
Indirect Cost <input type="text" name="cost_{$section.id}_cost" id="cost_$section.id}_cost" /> </label>

<label for="cost_{$section.id}_total">
<strong>Total</strong> <input type="text" name="cost_{$section.id}_total" id="cost_$section.id}_total" readonly="yes"/></label>


</fieldset>
</div>
<div class="block">
    <input type="submit" value="Send" name="btnSend" />
    <input type="submit" value="Cancel" name="btnReset" />
</div>
</form>
</div></div></div>
<div class="border-bl"><div class="border-br"><div class="border-bc"></div></div></div>
</div>
