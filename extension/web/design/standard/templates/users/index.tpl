<h1>Convocatorias</h1>

<table>
    <thead>
        <th>Category</th>
        <th>Cost</th>
        <th>Status</th>
    </thead>
    {def $data = fetch( 'web', 'get_categories_for_user', hash())}
    <tbody>
    {foreach $data as $cuser}
        <tr>
            <td><a href={concat( "web/category/", $cuser.id_cat)|ezurl}>{$cuser.category_name|wash}</a><td>
            <td>{$cuser.cost}<td>
            <td>{$cuser.status}<td>
        <tr>
    {/foreach}
    </tbody>
</table>
