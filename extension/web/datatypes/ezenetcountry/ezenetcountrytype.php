<?php

class eZEnetCountryType extends eZCountryType
{
    const DATA_TYPE_STRING = 'ezenetcountry';

    function eZEnetCountryType()
    {
        $this->eZDataType( self::DATA_TYPE_STRING, ezpI18n::tr( 'kernel/classes/datatypes', 'Euraxess Network Call Country', 'Datatype name' ),
                           array( 'serialize_supported' => true,
                                  'object_serialize_map' => array( 'data_text' => 'country' ) ) );
    }

    static function fetchCountryList()
    {
        if ( isset( $GLOBALS['EnetCountryList'] ) )
            return $GLOBALS['EnetCountryList'];

        $ini = eZINI::instance( 'enetcountry.ini' );
        $countries = $ini->getNamedArray();
        eZCountryType::fetchTranslatedNames( $countries );
        $GLOBALS['EnetCountryList'] = $countries;
        return $countries;
    }

}
eZDataType::register( eZEnetCountryType::DATA_TYPE_STRING, 'ezenetcountrytype' );
?>
